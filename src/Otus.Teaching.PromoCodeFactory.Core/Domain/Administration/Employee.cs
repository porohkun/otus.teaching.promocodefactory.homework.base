﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity, IMergeable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public bool Merge(object otherEmployee)
        {
            if (otherEmployee == null) return false;
            if (ReferenceEquals(this, otherEmployee)) return true;
            if (otherEmployee is Employee other)
            {
                if (this.Id != other.Id) return false;
                if (other.FirstName != null) this.FirstName = other.FirstName;
                if (other.LastName != null) this.LastName = other.LastName;
                if (other.Email != null) this.Email = other.Email;
                return true;
            }
            return false;
        }
    }
}