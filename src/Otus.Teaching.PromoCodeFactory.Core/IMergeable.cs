﻿namespace Otus.Teaching.PromoCodeFactory.Core
{
    public interface IMergeable
    {
        public bool Merge(object other);
    }
}
