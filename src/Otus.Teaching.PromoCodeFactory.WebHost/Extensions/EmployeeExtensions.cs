﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class EmployeeExtensions
    {
        public static EmployeeResponse ToResponse(this Employee employee) => new EmployeeResponse()
        {
            Id = employee.Id,
            Email = employee.Email,
            Roles = employee.Roles?.Select(x => new RoleItemResponse()
            {
                Name = x.Name,
                Description = x.Description
            }).ToList(),
            FullName = employee.FullName,
            AppliedPromocodesCount = employee.AppliedPromocodesCount
        };

        public static EmployeeShortResponse ToShortResponse(this Employee employee) => new EmployeeShortResponse()
        {
            Id = employee.Id,
            Email = employee.Email,
            FullName = employee.FullName,
        };

    }
}
