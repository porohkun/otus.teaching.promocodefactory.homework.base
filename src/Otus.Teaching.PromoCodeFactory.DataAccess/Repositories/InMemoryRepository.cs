﻿using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return Task.FromResult(entity.Id);
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            var result = await GetByIdAsync(id);
            if (!(result is default(T)))
                return await Task.FromResult(Data.Remove(result));
            return await Task.FromResult(false);
        }

        public async Task<bool> UpdateByIdAsync(T entity)
        {
            var oldEntity = await GetByIdAsync(entity.Id);
            if (oldEntity is default(T))
                return await Task.FromResult(false);
            if (oldEntity is IMergeable mergeableOldEntity)
                return await Task.FromResult(mergeableOldEntity.Merge(entity));
            return await Task.FromResult(false);
        }
    }
}